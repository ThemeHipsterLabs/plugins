<?php
// Load the TGM init if it exists
if ( file_exists( dirname( __FILE__ ) . '/tgm/tgm-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/tgm/tgm-init.php';
}
// Load the library for Importer
if ( file_exists( dirname( __FILE__ ) . '/importer-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/importer-init.php';
}
// Load the Importer if it exists
if ( file_exists( dirname( __FILE__ ) . '/importer.php' ) ) {
    require_once dirname( __FILE__ ) . '/importer.php';
}