<?php
/**
 * knockout - Stunning Wordpress Theme functions and definitions
 *
 * @package knockout - Stunning Wordpress Theme
 */

// Add Menu
add_action( 'admin_menu', 'knockout_register_importer_menu_page' );
function knockout_register_importer_menu_page(){
	add_menu_page( 'knockout', esc_html__( 'KnockOut', 'knockout'), 'manage_options', 'knockout_welcome','knockout_dashboard_page', get_template_directory_uri().'/assets/admin/img/logo-menu.png', 61 );
}
function knockout_dashboard_page(){
	echo '<h1>Here, KnockOut Theme.</h1>';
	echo '<p>Go to Appearance -> Customizer to custom theme.</p>';
}
// Add Sub Menu
add_action( 'admin_menu', 'knockout_sub_menu_importer' );
function knockout_sub_menu_importer() {
	add_submenu_page( 'knockout_welcome', esc_html__( 'Data Importer', 'knockout'), 'Importer', 'edit_theme_options', 'knockout_importer', 'knockout_importer_page', null, 1 );
}
// Dashboard
function knockout_importer_page(){
	$nonce = wp_create_nonce("knockout_import_nonce");
	?>
	<div class="wrap">
	    <h1 class="page-title"><?php _e( 'Import Demo Data', 'knockout') ?></h1>
	    <div id="welcome-panel" class="welcome-panel" style="padding: 20px;">
			<p class="tie_message_hint"><?php _e('Importing demo data (post, pages, images, theme settings, ...) is the easiest way to setup your theme. It will
		allow you to quickly edit everything instead of creating content from scratch. When you import the data following things will happen:','knockout');?></p>

			<ul style="list-style-position: inside;list-style-type: square;}">
			  	<li><?php _e( 'Works best to import on a new install of WordPress','knockout');?></li>
			  	<li><?php _e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.','knockout');?></li>
			  	<li><?php _e( 'No WordPress settings will be modified.','knockout');?></li>
			  	<li><?php _e( 'Posts, pages, some images, some widgets and menus will get imported.','knockout');?></li>
			  	<li><?php _e( 'Images will be downloaded from our server, these images are copyrighted and are for demo use only.','knockout');?></li>
			  	<li><?php _e( 'Please click import only once and wait, it can take a couple of minutes','knockout');?></li>
			</ul>
		</div>
	    <form method="post" action="" id="importContentForm">
	        <table class="form-table">
	            <tr>
	            	<td>
	            	    <p class="submit">
	            	        <input type="submit" name="submit" id="start_import" class="button button-primary" value="<?php _e( 'Start Import', 'knockout') ?>">
	            	        <span class="spinner" id="import_spinner" style="float: none;"></span>
	            	        <input type="hidden" name="nonce" id="nonce" value="<?php echo $nonce;?>">
	            	    </p>
					</td>
				</tr>
	        </table>
	    </form>
	</div>
	<?php
}
// Insert Javascript
add_action( 'admin_footer', 'knockout_import_javascript' ); // Write our JS below here

function knockout_import_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery("#start_import").live('click', function() {
			var demo_package = jQuery('#demo_package').val();
			var nonce = jQuery('#nonce').val();
			if (confirm('Do you want to import demo now?')) {
				jQuery(this).prop('disabled', true).next().addClass('is-active');
				jQuery.ajax({
					type 		: "post",
					dataType 	: "json",
					url 		: ajaxurl,
					data 		: {
						action: "knockout_import",
						nonce: nonce
					},
					success: function(getData) {
					    if(getData.type == "success") {
					    	jQuery("#start_import").prop('disabled', true).val( 'Imported' ).next().removeClass('is-active');
					    	jQuery("#start_import").before('<div class="updated notice is-dismissible rl-notice"><p>All Done. Have fun! Remember to Save Permalink, update the passwords and roles of imported users.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
					    }
					    else {
					       	alert(getData.message);
					    }
					}
				})
			}
			return false;
		});
	});
	</script> <?php
}
// Action for Ajax
add_action( 'wp_ajax_knockout_import', 'knockout_import_callback' );
function knockout_import_callback() {
	// Sercurity
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "knockout_import_nonce")) {
		exit("No naughty business please");
	}
	// Process
		ob_start();
	    $message = '';
		$knockout_import = new knockout_Theme_Demo_Data_Importer();
		$knockout_import->process_imports();
		$flag = $knockout_import->flag_as_imported;
		ob_get_clean();
		if ($flag['content'] == true && $flag['menus'] == true && $flag['widgets'] == true) {
			$result['type'] = "success";
			$result['message'] = '';
		}else{
			if ($flag['content'] == false) {
				$message .= __('Error on Content import.','knockout');
			}
			if ($flag['menus'] == false) {
				$message .= __('Error on Menus import.','knockout');
			}
			if ($flag['widgets'] == false) {
				$message .= __('Error on Widgets import.','knockout');
			}
			$result['type'] = "error";
			$result['message'] = $message;
		}
	// Result
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      	$result = json_encode($result);
      	echo $result;
      	die;
   	}
   	else {
    	header("Location: ".$_SERVER["HTTP_REFERER"]);
   	}

	wp_die();
}
