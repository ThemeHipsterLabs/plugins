<?php
/**
 * @package knockout
 */
/*
Plugin Name: HipsterLab
Plugin URI: http://hipsterlab.com/
Description: HipsterLab Business Core
Version: 1.0.0
Author: HipsterLab
Author URI: http://hipsterlab.com/
License: GPLv2 or later
Text Domain: knockout
*/

/**
 * Importer
 */
if ( file_exists( dirname( __FILE__ ) . '/install/install-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/install/install-init.php';
}